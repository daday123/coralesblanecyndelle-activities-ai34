import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css';
import Vue from 'vue'
import Dashboard from './Dashboard.vue'

import "chart.js"
import "hchs-vue-charts"

Vue.use(window.VueCharts);

Vue.config.productionTip = false

new Vue({
  render: h => h(Dashboard),
}).$mount('#app')
