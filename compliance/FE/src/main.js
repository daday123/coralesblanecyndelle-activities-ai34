import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'vue-toastification/dist/index.css';
import './assets/css/style.css';
import './assets/css/font-awesome/css/font-awesome.min.css';
import 'chart.js';
import 'hchs-vue-charts';
import store from './store';
import axios from 'axios';
import Vue from 'vue';
//import Login from './components/credentials/Login.vue';
import Dashboard from './components/pages/admin/Dashboard.vue';
import { router } from './assets/js/routes';
import Toast from "vue-toastification";

axios.defaults.baseURL = 'http://127.0.0.1:8000/';
Vue.use(axios);
Vue.use(Dashboard);
Vue.use(window.VueCharts);
Vue.config.productionTip = false;
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 10,
  newestOnTop: true
});

new Vue({
  store,
  router,
  render: h => h(Dashboard),
}).$mount('#app')
